" tilda's dots
" Made on Jul 31st, 2017 and improved ever since.

if !has('nvim')
    echom "This configuration is only for Neovim."
    echom "Please install Neovim and then try again."
    qa!
endif

" Begin init
if &compatible
  set nocompatible
endif
filetype off

set runtimepath+=~/.cache/dein/repos/github.com/Shougo/dein.vim

if dein#load_state('~/.cache/dein')
  call dein#begin('~/.cache/dein')
  " Dein manages itself via this line
  call dein#add('~/.cache/dein/repos/github.com/Shougo/dein.vim')
  " |
  " v Put plugins right below here

  " Time tracking
  call dein#add('wakatime/vim-wakatime') " Wakatime. Feel free to substitute with whatever.
  " Theme
  call dein#add('rakr/vim-one') " One (Light/Dark). This config uses One Dark.
  " Actual plugins for programming and shiz
  call dein#add('w0rp/ale') " ALE. Linter/fixer thing.
  call dein#add('othree/html5.vim') " HTML5 is better with this plugin
  call dein#add('tmhedberg/simpylfold') " Folding for Python.
  call dein#add('Shougo/deoplete.nvim') " Completion.
  call dein#add('Vimjas/vim-python-pep8-indent') " PEP8 indentation.
  call dein#add('mattn/emmet-vim') " Emmet!
  " The 2 plugins below are disabled by default. I don't use rust that much.
  " If you want to enable these, also see lines 57-59.
  " call dein#add('rust-lang/rust.vim') " Rust support.
  " call dein#add('racer-rust/vim-racer') " Racer completion support.
  " Etc
  call dein#add('aurieh/discord.nvim') " Discord integration.
  call dein#add('itchyny/lightline.vim') " Status bar.
  call dein#add('neoclide/vim-easygit') " It's a git plugin.
  call dein#add('scrooloose/nerdtree') " Files.
  call dein#add('mhinz/vim-startify') " Start screen. Because it's cool.

  " Extra stuff: don't touch
  call dein#end()
  call dein#save_state()
endif

" Plugin check for not installed plugins
if dein#check_install()
  call dein#install()
endif

" Rust settings (uncomment to enable):
" set hidden
" let g:racer_cmd = "~/.cargo/bin/racer"

" Theme.
filetype plugin indent on
if (has("nvim"))
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  if (has("termguicolors"))
    set termguicolors
endif
syntax on
set background=dark
let g:one_allow_italics = 1
colorscheme one

" Automatically enter Startify and open nerdtree if no args.
autocmd VimEnter *
	    \	if !argc()
	    \ |	  Startify
	    \ |   NERDTree
	    \ |   wincmd w
	    \ | endif

" Close vim if nerdtree is last window.
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Only open nerdtree on toggle (press F8).
map <F8> :NERDTreeToggle<CR>

" lightline config
let g:lightline = {
      \ 'colorscheme': 'one',
      \ }

" deoplete config!
let g:deoplete#enable_at_startup = 1

" vim-startify config
" some lines stolen from mhinz/dotfiles
let g:startify_custom_header = 'map(startify#fortune#boxed(), ''"   ".v:val'')'
let g:startify_fortune_use_unicode = 1

" ALE config
let g:ale_linters = {
\    'python': ['flake8'],
\}
let g:ale_fixers = {
\    'python': ['yapf']
\}

" Alt-<arrow> shortcuts
nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>

set number " line numbers.
set noshowmode " statusline shows mode
let g:easygit_enable_command = 1 " replace fugitive
